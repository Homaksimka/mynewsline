<%--
  Created by IntelliJ IDEA.
  User: homax
  Date: 08.04.17
  Time: 12:41
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Новостная лента</title>
</head>
<body>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<div align="center">
    <h1>Новости</h1>
    <%--<h2><a href="/new">New User</a></h2>--%>
    <select id="CategorySelect" size="1" style="width:150px">
        <option selected value="0">Выберите категорию
            <c:forEach var="category" items="${categoryList}">
                <c:if test="${categoryId == category.id}">
                    <option selected value=${category.id}>${category.name}</option>
                </c:if>
                <c:if test="${category.id != categoryId}">
                    <option value=${category.id}>${category.name}</option>
                </c:if>
            </c:forEach>
    </select>
    Заголовок: <input type="text" id="nameSearch"  value="${name}" />
    Содержание: <input type="text" id="contentSearch" value="${content}" />
    <button type="button" onclick="search();">Поиск</button>
    <table border="1">
        <tr>
        <th>Категория</th>
        <th>Заголовок</th>
        <th>Содержание</th>
        <th>Дата публикации</th>
        </tr>

        <c:forEach var="news" items="${listNews}">
            <tr>
                <td>${news.category.name}</td>
                <td>${news.name}</td>
                <td>${news.content}</td>
                <td>${news.dateOfPublishing.toString()}</td>
                <td>
                    <a href="/edit/${news.id}">Редактировать</a>
                    <a href="/delete/${news.id}">Удалить</a>
                </td>
            </tr>
        </c:forEach>
    </table>
    <form action="/create" method="get">
        <button type="submit" >Создать новую новость</button>
    </form>
</div>

<script>

        function search(){
            var categoryID = $('#CategorySelect').val();
            var name = $('#nameSearch').val();
            var content = $('#contentSearch').val();

            $.ajax({
                type : "GET",
                url : "/news",
                data: {categoryID: categoryID, name: name, content: content},
                timeout : 100000,
                success : function(data) {
                    console.log("SUCCESS: ", data);
                    $('body').html(data);
                },
                error : function(e) {
                    console.log("ERROR: ", e);

                },
                done : function(e) {
                    console.log("DONE");
                }
            });
        };

</script>

</body>
</html>
