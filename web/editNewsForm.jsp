<%--
  Created by IntelliJ IDEA.
  User: homax
  Date: 08.04.17
  Time: 17:50
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html>
<head>
    <title>Редактирование новости</title>
</head>
<body>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<div align="center">
    <h1>Новость</h1>
    <p id="id" hidden>${id}</p>
    <table>

            <tr>
                <td>Заголовок:</td>
                <td><textarea id="name">${name}</textarea></td>
            </tr>
            <tr>
                <td>Содеражние:</td>
                <td><textarea id="content">${content}</textarea></td>
            </tr>
            <tr>
                <td>Дата публикации:</td>
                <td><input type="datetime" id="date" value="${date}" readonly/></td>
            </tr>
            <tr>
                <td>Категория:</td>
                <td>
                    <select id="CategorySelect" size="1" style="width:150px">
                        <option selected value="0">Выберите категорию
                            <c:forEach var="category" items="${categoryList}">
                                <c:if test="${category.id == categoryId}">
                                    <option selected value=${category.id}>${category.name}</option>
                                </c:if>
                                <c:if test="${category.id != categoryId}">
                                    <option value=${category.id}>${category.name}</option>
                                </c:if>

                            </c:forEach>
                    </select>
                </td>
            </tr>

            <tr>
                <td colspan="2" align="right">
                    <button type="button" onclick="saveNews()">Сохранить</button>
                </td>
            </tr>

    </table>
</div>
</body>
<script type="text/javascript" >

    function saveNews() {

        var id = document.getElementById('id').innerText;
        var name = document.getElementById('name').value;
        var content = document.getElementById('content').value;
        var date = (document.getElementById('date').value == null || document.getElementById('date').value == ""
        ? null
        : document.getElementById('date').value);
        var categoryId = document.getElementById('CategorySelect').value;

        $.ajax({
            type: "POST",
            url: "/save",
            data: {id: id, name: name, content: content, date: date, categoryId: categoryId},
            dataType: 'json',
            timeout: 100000,
            success: function (data) {
                console.log("SUCCESS: ", data);
                alert("Успешно сохранено")
            },
            error: function (e) {
                console.log("ERROR: ", e);

            },
            done: function (e) {
                console.log("DONE");
            }
        });
    }

</script>

</html>
