package task.dao;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import task.models.News;

import java.util.List;

/**
 * Created by homax on 08.04.17.
 */
@Repository
public class NewsDaoImpl implements NewsDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public NewsDaoImpl(){

    }

    public NewsDaoImpl(SessionFactory sessionFactory){
        this.sessionFactory = sessionFactory;
    }

    @Override
    @Transactional
    public List<News> list(Integer categoryId, String name, String content) {

        List<News> listOfNews = null;
        StringBuilder sql = new StringBuilder("select * from News.News where 1=1");
        if(categoryId != null && categoryId != 0){
            sql.append(" and CategoryID = " + categoryId);
        }

        if(name != null && name != ""){
            sql.append(" and Name Like '%" + name + "%' ");
        }

        if(content != null && content != ""){
            sql.append(" and Content Like '%" + content + "%' ");
        }

        try {
            listOfNews =
                    sessionFactory
                            .getCurrentSession()
                            .createSQLQuery(sql.toString())
                            .addEntity(News.class)
                            .list();
        } catch (Exception ex){
            ex.printStackTrace();
        }

        return listOfNews;
    }

    @Override
    @Transactional
    public News getNews(int id) {

        News result = null;

        try {
            result = (News) sessionFactory
                    .getCurrentSession()
                    .load(News.class, id)
                    ;
        } catch (Exception ex){
            ex.printStackTrace();
        }

        return result;
    }

    @Override
    @Transactional
    public void saveOrUpdate(News news) {
        try {
            sessionFactory.getCurrentSession().saveOrUpdate(news);
        } catch (Exception ex){
            ex.printStackTrace();
        }
    }

    @Override
    @Transactional
    public void delete(News news) {
        try {
            sessionFactory.getCurrentSession().delete(news);
        } catch (Exception ex){
            ex.printStackTrace();
        }
    }
}
