package task.dao;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import task.models.Category;

import java.util.List;

/**
 * Created by homax on 08.04.17.
 */
@Repository
public class CategoryDaoImpl implements CategoryDAO {

    @Autowired
    SessionFactory sessionFactory;

    public CategoryDaoImpl(){

    }

    public CategoryDaoImpl(SessionFactory sessionFactory){
        this.sessionFactory = sessionFactory;
    }

    @Override
    @Transactional
    public List<Category> list() {

        List<Category> categoryList = null;

        try {
            categoryList =
                    sessionFactory
                            .getCurrentSession()
                            .createCriteria(Category.class)
                            .list()
                            ;
        } catch(Exception ex){
            ex.printStackTrace();
        }

        return categoryList;
    }

    @Override
    @Transactional
    public Category getCategory(int id) {

        Category result = null;

        try {
            result =
                    (Category) sessionFactory
                            .getCurrentSession()
                            .load(Category.class, id)
                            ;
        } catch (Exception ex){
            ex.printStackTrace();
        }

        return result;
    }

    @Override
    @Transactional
    public void saveOrUpdate(Category category) {
        try {
            sessionFactory.getCurrentSession().saveOrUpdate(category);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    @Transactional
    public void delete(Category category) {
        try{
            sessionFactory.getCurrentSession().delete(category);
        } catch (Exception ex){
            ex.printStackTrace();
        }
    }
}
