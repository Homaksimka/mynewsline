package task.dao;

import task.models.Category;

import java.util.List;

/**
 * Created by homax on 08.04.17.
 */
public interface CategoryDAO {

    public List<Category> list();

    public Category getCategory(int id);

    public void saveOrUpdate(Category category);

    public void delete(Category category);
}
