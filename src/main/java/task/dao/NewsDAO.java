package task.dao;

import task.models.News;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by homax on 08.04.17.
 */
public interface NewsDAO {

    public List<News> list(Integer categoryId, String name, String content);

    public News getNews(int id);

    public void saveOrUpdate(News news);

    public void delete(News news);

}
