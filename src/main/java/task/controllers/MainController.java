package task.controllers;

import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import task.dao.CategoryDAO;
import task.dao.NewsDAO;
import task.models.Category;
import task.models.News;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by homax on 07.04.17.
 */

@Controller
public class MainController {

    @Qualifier("newsDaoImpl")
    @Autowired
    private NewsDAO newsDAO;

    @Qualifier("categoryDaoImpl")
    @Autowired
    private CategoryDAO categoryDAO;

    @RequestMapping(value="/news", method= RequestMethod.GET)
    public String news(@RequestParam(name = "categoryID", required = false) Integer categoryid,
                       @RequestParam(name = "name", required = false) String name,
                       @RequestParam(name = "content", required = false) String content,
                       ModelMap model){

        model.addAttribute("name", name);
        model.addAttribute("content", content);
        model.addAttribute("categoryId", categoryid == null ? 0 : categoryid);

        List<News> listNews = newsDAO.list(categoryid, name, content);
        model.addAttribute("listNews", listNews);

        List<Category> categoryList = categoryDAO.list();
        model.addAttribute("categoryList", categoryList);

        return "/newsForm";
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    public ModelAndView delete(@PathVariable("id") int id){

        News newsDelete = new News();
        newsDelete.setId(id);
        newsDAO.delete(newsDelete);

        return new ModelAndView("redirect:/news");
    }

    @RequestMapping(value= "/create", method=RequestMethod.GET)
    public String createNews(ModelMap model)
    {
        model.addAttribute("categoryId", 0);

        List<Category> categoryList = categoryDAO.list();
        model.addAttribute("categoryList", categoryList);

        return "/editNewsForm";
    }
    @RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
    public String edit(@PathVariable("id") int id, ModelMap model){

        News news = newsDAO.getNews(id);
        model.addAttribute("id", news.getId());
        model.addAttribute("name", news.getName());
        model.addAttribute("content", news.getContent());
        model.addAttribute("date", news.getDateOfPublishing().toString());
        model.addAttribute("categoryId", news.getCategory().getId());

        List<Category> categoryList = categoryDAO.list();
        model.addAttribute("categoryList", categoryList);

        return "/editNewsForm";
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public @ResponseBody
    String save(
            @RequestParam(name = "id") Integer id,
            @RequestParam(name = "name") String name,
            @RequestParam(name = "content") String content,
            @RequestParam(name = "date") String date,
            @RequestParam(name = "categoryId") int categoryId){


        News news = new News();
        news.setId(id == null ? 0 : id);
        news.setName(name);
        news.setContent(content);

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        try {
            Date dateParsed;
            if(date == null || date == "") {
                dateParsed = new Date();
            }
            else{
                dateParsed = simpleDateFormat.parse(date);
            }
            news.setDateOfPublishing(dateParsed);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Category category = categoryDAO.getCategory(categoryId);
        news.setCategory(category);

        newsDAO.saveOrUpdate(news);

        return JSONObject.quote("Success");
    }


}
