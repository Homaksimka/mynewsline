package task.models;

import org.hibernate.annotations.Proxy;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by homax on 08.04.17.
 */
@Entity
@Proxy(lazy = false)
@Table(name = "Category")
public class Category {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private int id;

    @Column(name = "Name")
    private String name;

    @OneToMany(mappedBy = "category")
    private Set<News> newsSet;

    public int getId(){
        return  id;
    }

    public String getName(){
        return name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }
}
