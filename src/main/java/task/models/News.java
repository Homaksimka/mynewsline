package task.models;

import org.hibernate.annotations.Proxy;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by homax on 08.04.17.
 */
@Entity
@Proxy(lazy = false)
@Table(name = "News")
public class News {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private int id;

    @Column(name = "Name")
    private String name;

    @Column(name = "Content")
    private String content;

    @Column(name = "dateOfPublishing")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date dateOfPublishing;

    @ManyToOne
    @JoinColumn(name = "CategoryID")
    private Category category;

    public int getId(){
        return id;
    }


    public String getName(){
        return name;
    }


    public String getContent(){
        return content;
    }


    public Date getDateOfPublishing(){
        return dateOfPublishing;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setDateOfPublishing(Date dateOfPublishing) {
        this.dateOfPublishing = dateOfPublishing;
    }
}
